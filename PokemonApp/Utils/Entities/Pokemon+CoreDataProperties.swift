//
//  Pokemon+CoreDataProperties.swift
//  PokemonApp
//
//  Created by Juan Montiel on 07/07/21.
//

import Foundation
import CoreData

extension Pokemon {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pokemon> {
        return NSFetchRequest<Pokemon>(entityName: "Pokemon")
    }

    @NSManaged public var name: String?
    @NSManaged public var url: String?

}
