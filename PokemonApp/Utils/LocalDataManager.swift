//
//  LocalDataManager.swift
//  PokemonApp
//
//  Created by Juan Montiel on 07/07/21.
//

import UIKit
import CoreData

class LocalDataManager {
    static let sharedInstance = LocalDataManager()

    func saveTransaction(pokemon: PokemonModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Pokemon", in: context) else {
            return
        }
        
        guard let dbTransaction = NSManagedObject(entity: entity, insertInto: context) as? Pokemon else {
            return
        }
        
        dbTransaction.name = pokemon.name
        dbTransaction.url = pokemon.url
        
        do {
            try context.save()
        } catch let error {
            print("No ha sido posible guardar la notificación\(error.localizedDescription)")
        }
    }
    
    func getPokemons() -> [PokemonModel] {
        var pokemons: [PokemonModel] = []
        
        getRaw( completionHandler: { (values) in
            
            for pokemon in values {
                pokemons.append(PokemonModel(pokemon: pokemon))
            }
        })
        return pokemons
    }
    
    private func getRaw(completionHandler: @escaping(_ pokemon:[Pokemon]) -> Void ) {
        var rawPokemon:[Pokemon] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext
        
        let fetch: NSFetchRequest<Pokemon> = Pokemon.fetchRequest()

        do {
            rawPokemon = try context.fetch(fetch)
        } catch {
            print("get notification error")
        }
        
        completionHandler(rawPokemon)
    }
    
    func deleteData() {
        let appDel:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context:NSManagedObjectContext = appDel.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pokemon")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for managedObject in results {
                if let managedObjectData: NSManagedObject = managedObject as? NSManagedObject {
                    context.delete(managedObjectData)
                }
            }
        } catch {
        }
    }
    
}
