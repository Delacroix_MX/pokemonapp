//
//  PokemonResponse.swift
//  PokemonApp
//
//  Created by Juan Montiel on 07/07/21.
//

import UIKit
import ObjectMapper

class PokemonResponse: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        results <- map["results"]

   }
   
    var count: Int?
    var next: String?
    var previous: String?
    var results: [PokemonModel]?
}

