//
//  PokemonModel.swift
//  PokemonApp
//
//  Created by Juan Montiel on 07/07/21.
//

import UIKit
import ObjectMapper

class PokemonModel: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        url <- map["url"]
   }
    
    init(pokemon: Pokemon) {
        self.name = pokemon.name
        self.url = pokemon.url
    }
   
    var name: String?
    var url: String?
}
