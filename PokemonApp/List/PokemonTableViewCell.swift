//
//  PokemonTableViewCell.swift
//  PokemonApp
//
//  Created by Juan Montiel on 07/07/21.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    var item:PokemonModel? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell() {
        guard let item = item  else {
            return
        }

        img.image = nil
        nameLabel.text = item.name
        let value = item.url?.split(separator: "/").last
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(value!).png"
        img.downloadedFrom(link: url, contentMode: .scaleAspectFill)
    }
}
